export default class Array2 {

  constructor(width, height) {
    this._width = width;
    this._height = height;
    this._array = [];

    for(var i = 0; i < this._width * this._height; i++) {
      this._array[i] = null;
    }
  }

  get width() {
    return this._width;
  }

  get height() {
    return this._height;
  }

  set(x, y, value) {
    if(!this.contains(x, y)) throw new Error('Out of Bounds');
    this._array[this.index(x, y)] = value;
  }

  get(x, y) {
    if(!this.contains(x, y)) throw new Error('Out of Bounds');
    return this._array[this.index(x, y)];
  }

  move(fromX, fromY, toX, toY) {
    if(!this.contains(fromX, fromY)) throw new Error('Out of Bounds');
    if(!this.contains(toX, toY)) throw new Error('Out of Bounds');
    const value = this.get(fromX, fromY);
    this.set(toX, toY, value);
    this.set(fromX, fromY, null);
  }

  contains(x, y) {
    if(x < 0 || x >= this._width) return false;
    if(y < 0 || y >= this._height) return false;
    return true;
  }

  index(x, y) {
    if(!this.contains(x, y)) throw new Error('Out of Bounds');
    return y * this._width + x;
  }

}
