export default class Storage {
  constructor() {
    this._available = Phaser.Device.localStorage;
    this._storage = localStorage;
  }

  destroy() {
    this._available = false;
    this._storage = null;
  }

  get(key) {
    if(!this._available) {
      return null;
    }

    return this._storage.getItem(key);
  }

  set(key, value) {
    if(!this._available) {
      return;
    }

    this._storage.setItem(key, value);
  }
}
