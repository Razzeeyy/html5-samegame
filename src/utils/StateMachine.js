export default class StateMachine {
  constructor() {
    this._states = {};
    this._currentState = null;
  }

  addState(name, state) {
    if(!name) throw new Error('Invalid Name');
    if(!state) throw new Error('Invalid State');
    if(this._states[name]) throw new Error('State Already Exists: '+name);
    this._states[name] = state;
  }

  setState(name) {
    if(!this._states[name]) throw new Error('State Not Found: '+name);
    this._currentState = this._states[name];
  }

  getState() {
    return this._currentState;
  }
}
