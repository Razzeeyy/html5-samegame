export default {
  scoreIndicator:  { font: 'bold 32px sans-serif', fill: 'black', align: 'center' },
  score: { font: 'bold 32px sans-serif', fill: 'black', align: 'center' },
  currentScore: { font: 'bold 32px sans-serif', fill: 'green', align: 'center' }
};
