export default class BoardViewFallState {
  constructor() {

  }

  update(board) {
    for(let x=0; x<board._tiles.width; x++) { //fall the pieces
      for(let y=board._tiles.height-1; y>0; y--) {
        if(!board._tiles.get(x, y)) { //TODO: refactor fall logic
          let offset=1;
          while(board._tiles.contains(x, y-offset) && !board._tiles.get(x, y-offset)) {
            offset++;
          }
          if(board._tiles.contains(x, y-offset) && board._tiles.get(x, y-offset)) {
            board.fallTile(x, y-offset, y);
          }
        }
      }
    }

    board._stateMachine.setState('slide');
  }

}
