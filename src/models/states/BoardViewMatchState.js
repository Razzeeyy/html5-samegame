export default class BoardViewMatchState {
  constructor() {

  }

  update(board) {
    const input = board._input;

    const matched = board.floodfill(input.x, input.y);

    if(!matched || matched.length < 2) return;

    board.events.onMatch.dispatch(matched.length);

    matched.forEach(tile => {
      board.removeTile(tile.x, tile.y);
    });

    board._input = null;

    board._stateMachine.setState('fall');
  }

}
