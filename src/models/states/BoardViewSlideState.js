export default class BoardViewSlideState {
  constructor() {

  }

  update(board) {
    let firstEmptyX = null;
    let slideDist = null;

    for(let x=0; x<board._tiles.width; x++) { //check if we need to slide pieces left
      const tile = board._tiles.get(x, board._tiles.height-1);
      if(firstEmptyX === null && !tile) { //TODO: refactor slide logic
        firstEmptyX = x;
      } else if(tile && firstEmptyX !== null && slideDist === null) {
        slideDist = tile.x - firstEmptyX;
      } else if(slideDist !== null) {
        break;
      }
    }

    if(firstEmptyX !== null && slideDist !== null) { //we need to slide
      for(let y=0; y<board._tiles.height; y++) {
        for(let x=firstEmptyX+slideDist; x<board._tiles.width; x++) {
          const tile = board._tiles.get(x, y); //TODO: refactor slide logic
          if(tile) {
            board.slideTile(x, y, x-slideDist);
          }
        }
      }
    }

    board._stateMachine.setState('idle');
  }

}
