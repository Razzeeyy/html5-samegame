export default class BoardViewIdleState {
  constructor() {

  }

  update(board) {
    if(!board.hasMove()) {
      board.events.onNoMoreMoves.dispatch();
      board._stateMachine.setState('gameover');
    }

    if(board._input) {
      board._stateMachine.setState('match');
    }
  }

}
