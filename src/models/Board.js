import {default as Array2} from '../utils/Array2';
import Tile from './Tile';
import StateMachine from '../utils/StateMachine';

import BoardViewIdleState from './states/BoardViewIdleState';
import BoardViewMatchState from './states/BoardViewMatchState';
import BoardViewFallState from './states/BoardViewFallState';
import BoardViewSlideState from './states/BoardViewSlideState';
import BoardViewGameOverState from './states/BoardViewGameOverState';

export default class Board {

  constructor(width, height) {
    this._tiles = new Array2(width, height);

    this._input = null;

    this._stateMachine = new StateMachine();
    this._stateMachine.addState('idle', new BoardViewIdleState());
    this._stateMachine.addState('match', new BoardViewMatchState());
    this._stateMachine.addState('fall', new BoardViewFallState());
    this._stateMachine.addState('slide', new BoardViewSlideState());
    this._stateMachine.addState('gameover', new BoardViewGameOverState());

    this._stateMachine.setState('idle');

    this.events = {
      onTileAdd: new Phaser.Signal(),
      onTileFall: new Phaser.Signal(),
      onTileSlide: new Phaser.Signal(),
      onTileRemove: new Phaser.Signal(),
      onMatch: new Phaser.Signal(),
      onNoMoreMoves: new Phaser.Signal()
    };
  }

  update() {
    this._stateMachine.getState().update(this);
  }

  destroy() {
    Object.entries(this.events).forEach(([, signal]) => signal.dispose());
  }

  addTile(x, y, tile) {
    this._tiles.set(x, y, tile);
    this.events.onTileAdd.dispatch(x, y, tile.color);
  }

  fallTile(x, y, toY) {
    this.move(x, y, x, toY);
    this.events.onTileFall.dispatch(x, y, toY);
  }

  slideTile(x, y, toX) {
    this.move(x, y, toX, y);
    this.events.onTileSlide.dispatch(x, y, toX);
  }

  removeTile(x, y) {
    this._tiles.set(x, y, null);
    this.events.onTileRemove.dispatch(x, y);
  }

  move(x, y, toX, toY) {
    const tile = this._tiles.get(x, y);
    this._tiles.set(x, y, null);
    tile.x = toX;
    tile.y = toY;
    this._tiles.set(toX, toY, tile);
  }

  clean(dispatchRemove, force) {
    for(let x=0; x<this._tiles.width; x++) {
      for(let y=0; y<this._tiles.height; y++) {
        if(dispatchRemove) {
          if(this._tiles.get(x, y) || force) {
            this.removeTile(x, y);
          }
        } else {
          this._tiles.set(x, y, null);
        }
      }
    }
    this._stateMachine.setState('idle');
  }

  populate(colors) {
    for(let x=0; x<this._tiles.width; x++) {
      for(let y=0; y<this._tiles.height; y++) {
        const color = Phaser.ArrayUtils.getRandomItem(colors);
        this.addTile(x, y, new Tile(x, y, color));
      }
    }
  }

  get width() {
    return this._tiles.width;
  }

  get height() {
    return this._tiles.height;
  }

  floodfill(x, y, result, color) {
    if(!this._tiles.contains(x, y)) return null;

    const tile = this._tiles.get(x, y);

    if(!tile) return null;

    if(!result) result = [];
    if(!color) color = tile.color;

    if(tile.color == color && !result.includes(tile)){
      result.push(tile);
      this.floodfill(x+1, y, result, color);
      this.floodfill(x-1, y, result, color);
      this.floodfill(x, y+1, result, color);
      this.floodfill(x, y-1, result, color);
    }

    return result;
  }

  hasMove() {
    for(let x=0; x<this._tiles.width; x++) {
      for(let y=0; y<this._tiles.height; y++) {
        const tile = this._tiles.get(x, y);
        if(tile) {
          const tileRight = this._tiles.contains(x+1, y) ? this._tiles.get(x+1, y) : null;
          if(tileRight && tileRight.color == tile.color) {
            return  true;
          }
          const tileDown = this._tiles.contains(x, y+1) ? this._tiles.get(x, y+1) : null;
          if(tileDown && tileDown.color == tile.color) {
            return true;
          }
        }
      }
    }
    return false;
  }

  match(x, y) {
    this._input = {x: x, y: y};
  }

}
