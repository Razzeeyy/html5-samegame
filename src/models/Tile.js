export default class Tile {

  constructor(x, y, color) {
    this.x = x;
    this.y = y;
    this.color = color;
  }

  clone() {
    return new Tile(this.x, this.y, this.color);
  }

  equals(other) {
    return other.x == this.x && other.y == this.y && other.color == this.color;
  }
}
