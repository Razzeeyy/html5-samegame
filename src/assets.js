/*
 * `assets` module
 * ===============
 *
 * Declares static asset packs to be loaded using the `Phaser.Loader#pack`
 * method. Use this module to declare game assets.
 */

export default {
  // -- Splash screen assets used by the Preloader.
  boot: [{
    key: 'progress-bar',
    url: '1x1.png',
    type: 'image'
  }],

  // -- General assets used throughout the game.
  game: [{
    key: 'play',
    type: 'image'
  }, {
    key: 'tile',
    url: 'tile.png',
    type: 'image'
  }, {
    key: 'replay',
    url: 'refresh.png',
    type: 'image'
  }, {
    key: 'tint',
    url: '1x1.png',
    type: 'image'
  }, {
    key: 'click',
    urls: 'button.wav',
    type: 'audio'
  }, {
    key: 'click',
    urls: 'button.ogg',
    type: 'audio'
  }, {
    key: 'click',
    urls: 'button.mp3',
    type: 'audio'
  }, {
    key: 'match',
    urls: 'match.wav',
    type: 'audio'
  }, {
    key: 'match',
    urls: 'match.ogg',
    type: 'audio'
  }, {
    key: 'match',
    urls: 'match.mp3',
    type: 'audio'
  }, {
    key: 'replayClick',
    urls: 'replay.wav',
    type: 'audio'
  }, {
    key: 'replayClick',
    urls: 'replay.ogg',
    type: 'audio'
  }, {
    key: 'replayClick',
    urls: 'replay.mp3',
    type: 'audio'
  }]
};
