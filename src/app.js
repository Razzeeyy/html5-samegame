/*
 * `app` module
 * ============
 *
 * Provides the game initialization routine.
 */

// Required: import the Babel runtime module.
import 'babel-polyfill';

import field from './field';

// Import game states.
import * as states from './states';

export function init() {
  const game = new Phaser.Game(field.width*field.tileSize, field.height*field.tileSize, Phaser.AUTO);

  // Dynamically add all required game states.
  Object
    .entries(states)
    .forEach(([key, state]) => game.state.add(key, state));

  game.state.start('Boot');

  return game;
}
