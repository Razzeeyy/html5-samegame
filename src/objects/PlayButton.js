export default class ReplayButton extends Phaser.Button {
  constructor(game, clickedCallback, clickedCallbackContext) {
    super(game, 0, 0, 'play', clickedCallback, clickedCallbackContext);

    this.events.onInputOver.add(this._handleOver, this);
    this.events.onInputOut.add(this._handleOut, this);

    this.anchor.x = 0.5;
    this.anchor.y = 0.5;
  }

  _handleOver() {
    const tween = this.game.add.tween(this);
    tween.to({width: this.width*1.25, height: this.height*1.25}, 200, Phaser.Easing.Linear.None, true);
  }

  _handleOut() {
    const tween = this.game.add.tween(this);
    tween.to({width: this.width*0.8, height: this.height*0.8}, 200, Phaser.Easing.Linear.None, true);
  }
}
