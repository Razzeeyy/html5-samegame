import fonts from '../fonts';

export default class ScoreCounter extends Phaser.Text {

  constructor(game, board, x, y) {
    super(game, x, y, '0', fonts.scoreIndicator);
    this._board = board;

    this._score = 0;
    this._board.events.onMatch.add(this.adjustScore, this);
  }

  destroy(destroyChildren) {
    super.destroy(destroyChildren);

    this._board.events.onMatch.remove(this.adjustScore, this);
    this._score = 0;
  }

  get score() {
    return this._score;
  }

  adjustScore(matchLength) {
    const matchScore = Math.pow(matchLength-1, 2);
    this._score += matchScore;

    this.text = this._score;
  }
}
