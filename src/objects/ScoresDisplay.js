import fonts from '../fonts';

export default class ScoresDisplay extends Phaser.Group {
  constructor(game, recordScore, currentScore) {
    super(game);

    const score = new Phaser.Text(this.game, 0, 0, currentScore, fonts.currentScore);
    score.anchor.x = 0.5;
    score.anchor.y = 0.5;
    this.addChild(score);

    const storedScore = new Phaser.Text(this.game, 0, 0, recordScore, fonts.score);
    storedScore.anchor.x = 0.5;
    storedScore.anchor.y = 0.5;
    this.addChild(storedScore);

    storedScore.x = score.x;

    if(currentScore > recordScore) {
      score.y = 0;
      storedScore.y += score.height*1.25;

      const tween = this._scoreTween = this.game.add.tween(score.scale);
      tween.to({x: 1.15, y: 1.15}, 1000, Phaser.Easing.Linear.None, true, 0, -1, true);
    } else {
      storedScore.y = 0;
      score.y += storedScore.height*1.25;

      const tween = this._scoreTween = this.game.add.tween(score);
      tween.to({alpha: 0.5}, 300, Phaser.Easing.Linear.None, true, 0, 3, true);
    }

  }

  destroy(destroyChildren, soft) {
    super.destroy(destroyChildren, soft);

    if(this._scoreTween) {
      this._scoreTween.stop();
    }
  }

}
