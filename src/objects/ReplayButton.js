export default class ReplayButton extends Phaser.Button {
  constructor(game, clickedCallback, clickedCallbackContext) {
    super(game, 0, 0, 'replay');

    this.events.onInputUp.add(this._handleClick, this);

    this.events.onInputOver.add(this._handleOver, this);
    this.events.onInputOut.add(this._handleOut, this);

    this._clickedCallback = clickedCallback;
    this._clickedCallbackContext = clickedCallbackContext;

    this.anchor.x = 0.5;
    this.anchor.y = 0.5;
  }

  _handleOver() {
    const tween = this.game.add.tween(this.scale);
    tween.to({x: 1.25, y: 1.25}, 200, Phaser.Easing.Linear.None, true);
  }

  _handleOut() {
    const tween = this.game.add.tween(this.scale);
    tween.to({x: 1, y: 1}, 200, Phaser.Easing.Linear.None, true);
  }

  _handleClick() {
    const tween = this.game.add.tween(this);
    tween.to({angle: -360*4}, 1000, Phaser.Easing.Linear.In, true);
    tween.onComplete.add(this._clickedCallback, this._clickedCallbackContext);
  }
}
