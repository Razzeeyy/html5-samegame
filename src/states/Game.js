/*
 * Game state
 * ==========
 *
 * A sample Game state, displaying the Phaser logo.
 */

import field from '../field';
import colors from '../colors';
import Board from '../models/Board';
// import {default as BoardView} from '../views/ImmediateBoardView';
import {default as BoardView} from '../views/BoardView';
import ScoreCounter from '../objects/ScoreCounter';

export default class Game extends Phaser.State {

  create() {
    this.stage.backgroundColor = 0xFFFFFF;

    const FIELD_WIDTH = field.width;
    const FIELD_HEIGHT = field.height;
    const TILE_SIZE = field.tileSize;

    this.FIELD_PIXEL_WIDTH = FIELD_WIDTH*TILE_SIZE;
    this.FIELD_PIXEL_HEIGHT = FIELD_HEIGHT*TILE_SIZE;

    const board = this.board = new Board(FIELD_WIDTH, FIELD_HEIGHT);

    board.events.onNoMoreMoves.add(this.switchToGameOver, this);
    board.events.onMatch.add(this.playMatchSound, this);

    const boardView = this.boardView = new BoardView(this.game, board, TILE_SIZE);

    board.populate(colors);

    boardView.width = this.FIELD_PIXEL_WIDTH;
    boardView.height = this.FIELD_PIXEL_HEIGHT;

    this.add.existing(boardView);

    const scoreCounter = this.scoreCounter = new ScoreCounter(this.game, board, 0, 0);
    this.add.existing(scoreCounter);

    this.matchSound = new Phaser.Sound(this.game, 'match');

    this.resize();
  }

  update() {
    this.board.update();
  }

  resize() {
    this.scoreCounter.x = this.scoreCounter.width*0.5;
    this.scoreCounter.y = this.scoreCounter.width*0.25;
  }

  switchToGameOver() {
    this.board.events.onNoMoreMoves.remove(this.switchToGameOver, this);
    this.board.events.onMatch.remove(this.playMatchSound, this);
    this.board.destroy();
    this.scoreCounter.kill();
    this.boardView.setInputActive(false);

    this.state.start('GameOver', true, false, this.scoreCounter.score, this.board._tiles);
  }

  playMatchSound() {
    this.matchSound.play();
  }
}
