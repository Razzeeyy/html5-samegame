import Storage from '../utils/Storage';
import ScoresDisplay from '../objects/ScoresDisplay';
import ReplayButton from '../objects/ReplayButton';

import field from '../field';
import StaticBoardView from '../views/StaticBoardView';

export default class GameOver extends Phaser.State {

  init(score, tiles) {
    this._score = score;
    this._tiles = tiles;
  }

  create() {
    this.stage.backgroundColor = 0xFFFFFF;

    this.staticBoardView = new StaticBoardView(this.game, this._tiles, field.tileSize);
    this.add.existing(this.staticBoardView);

    this.tint = new Phaser.Image(this.game, 0, 0, 'tint');
    this.tint.alpha = 0;
    const tintFade = this.game.add.tween(this.tint);
    tintFade.to({alpha: 0.3}, 1000, Phaser.Easing.Linear.None, true);
    this.add.existing(this.tint);

    const storage = this._storage = new Storage();

    this._storedScore = storage.get('hiscore');

    if(this._score > this._storedScore) {
      storage.set('hiscore', this._score);
    }

    this.scores = new ScoresDisplay(this.game, this._storedScore, this._score);

    this.replay = new ReplayButton(this.game, this.switchToGame, this);
    this.add.existing(this.replay);

    const click = this.click = new Phaser.Sound(this.game, 'replayClick');
    this.replay.setDownSound(click);

    this.resize();
  }

  resize() {
    this.tint.width = this.world.width;
    this.tint.height = this.world.height;

    this.scores.x = (this.world.width-this.scores.width)*0.606;
    this.scores.y = (this.world.height-this.scores.height)*0.25;

    this.replay.alignIn(this.world.bounds, Phaser.BOTTOM_CENTER);
    this.replay.y = this.world.height*0.66;
  }

  switchToGame() {
    this._storage.destroy();
    this.state.start('Game');
  }

}
