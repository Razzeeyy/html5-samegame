import field from '../field';
import colors from '../colors';
import Board from '../models/Board';
// import {default as BoardView} from '../views/ImmediateBoardView';
import {default as BoardView} from '../views/BoardView';
import PlayButton from '../objects/PlayButton';


export default class Menu extends Phaser.State {

  create() {
    this.stage.backgroundColor = 0xFFFFFF;
    this.game.sound.volume = 0.25;

    const board = this.board = new Board(field.width, field.height);

    const boardView = this.boardView = new BoardView(this.game, board, field.tileSize);
    boardView.width = field.width*field.tileSize;
    boardView.height = field.height*field.tileSize;
    this.add.existing(boardView);

    board.populate(colors);

    this.tint = new Phaser.Image(this.game, 0, 0, 'tint');
    const tintFade = this.game.add.tween(this.tint);
    tintFade.to({alpha: 0.3}, 1000, Phaser.Easing.Linear.None, true);
    this.add.existing(this.tint);


    this.play = new PlayButton(this.game, this.fadeAndSwitch, this);
    this.add.existing(this.play);

    const click = this.click = new Phaser.Sound(this.game, 'click');
    this.play.setDownSound(click);

    this.resize();
  }

  update() {
    const board = this.board;
    const boardView = this.boardView;

    boardView.setInputActive(false);

    if(!boardView.isAnimating()) {
      if(board.hasMove()) {
        const x = Math.floor(Math.random()*board.width);
        const y = Math.floor(Math.random()*board.height);
        board.match(x, y);
      } else {
        board.clean(true);
        board.populate(colors);
      }
    }

    board.update();
  }

  resize() {
    this.tint.width = this.world.width;
    this.tint.height = this.world.height;

    this.play.alignIn(this.world.bounds, Phaser.CENTER);
  }

  fadeAndSwitch() {
    const tintFade = this.game.add.tween(this.tint);
    tintFade.to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    tintFade.onComplete.add(this.switchToGame, this);
  }

  switchToGame() {
    this.board.destroy();

    this.state.start('Game');
  }

}
