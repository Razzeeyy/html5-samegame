/*
 * Preloader state
 * ===============
 *
 * Takes care of loading the main game assets, including graphics and sound
 * effects, while displaying a busy splash screen.
 */

import assets from '../assets';

export default class Preloader extends Phaser.State {

  preload() {
    this.progress = this.add.image(0, 0, 'progress-bar'); //show loading bar
    this.resize();
    this.load.setPreloadSprite(this.progress);

    this.load.pack('game', null, assets);
  }

  create() {
    // Here is a good place to initialize plugins dependent of any game asset.
    // Don't forget to `import` them first. Example:
    //this.game.myPlugin = this.plugins.add(MyPlugin/*, ... parameters ... */);
    this.time.desiredFps=120;

    this.state.start('Menu');
  }

  resize() {
    this.progress.width = this.world.width;
    this.progress.height = this.world.height/10;
    this.progress.alignIn(this.world.bounds, Phaser.CENTER);
  }

}
