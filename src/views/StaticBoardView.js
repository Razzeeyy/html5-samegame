import TileView from './TileView';

export default class StaticBoardView extends Phaser.Group {

  constructor(game, tiles, tileSize) {
    super(game);

    for(var x=0; x<tiles.width; x++) {
      for(var y=0; y<tiles.height; y++) {
        const tile = tiles.get(x, y);
        if(tile) {
          const tileView = new TileView(this.game, x, y, tile.color, tileSize);
          tileView.input.enabled = false;
          this.addChild(tileView);
        }
      }
    }
  }
}
