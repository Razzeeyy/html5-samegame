import Action from './Action';

export default class SlideTileAction extends Action {

  constructor(boardView, x, y, toX) {
    super(boardView, 'SlideTileAction');
    this.x = x;
    this.y = y;
    this.toX = toX;
  }

  run(completeCallback, completeCallbackContext) {
    const tile = this.boardView._tiles.get(this.x, this.y);
    this.boardView._tiles.set(this.x, this.y, null);
    tile.move(this.toX, this.y, 250, Phaser.Easing.Bounce.Out).onComplete.add(completeCallback, completeCallbackContext);
    this.boardView._tiles.set(this.toX, this.y, tile);
  }

}
