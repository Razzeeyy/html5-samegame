import Action from './Action';

export default class RemoveTileAction extends Action {

  constructor(boardView, x, y) {
    super(boardView, 'RemoveTileAction');
    this.x = x;
    this.y = y;
  }

  run(completeCallback, completeCallbackContext) {
    const tile = this.boardView._tiles.get(this.x, this.y);
    this.boardView._tiles.set(this.x, this.y, null);

    const tween = tile.fade(1, 0, 200, Phaser.Easing.Linear.None);
    tween.onComplete.add(this._tweenComplete, this);
    tween.onComplete.add(completeCallback, completeCallbackContext);
  }

  _tweenComplete(tile) {
    this.boardView.remove(tile, true);
  }
}
