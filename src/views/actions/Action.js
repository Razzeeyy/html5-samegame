//just an example of action interface

export default class Action {

  constructor(boardView, actionType) {
    this.boardView = boardView;
    this.type = actionType;
  }

  run(completeCallback, completeCallbackContext) {
    completeCallback.call(completeCallbackContext);
  }

}
