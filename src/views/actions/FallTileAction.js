import Action from './Action';

export default class FallTileAction extends Action {

  constructor(boardView, x, y, toY) {
    super(boardView, 'FallTileAction');
    this.x = x;
    this.y = y;
    this.toY = toY;
  }

  run(completeCallback, completeCallbackContext) {
    const tile = this.boardView._tiles.get(this.x, this.y);
    this.boardView._tiles.set(this.x, this.y, null);
    tile.move(this.x, this.toY, 200, Phaser.Easing.Linear.None).onComplete.add(completeCallback, completeCallbackContext);
    this.boardView._tiles.set(this.x, this.toY, tile);
  }

}
