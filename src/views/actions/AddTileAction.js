import Action from './Action';
import TileView from '../TileView';

export default class AddTileAction extends Action {

  constructor(boardView, x, y, color) {
    super(boardView, 'AddTileAction');
    this.x = x;
    this.y = y;
    this.color = color;
  }

  run(completeCallback, completeCallbackContext) {
    const tile = new TileView(this.boardView.game,
      this.x, this.y, this.color, this.boardView._TILE_SIZE,
      this.boardView.match, this.boardView);

    this.boardView._tiles.set(this.x, this.y, tile);
    this.boardView.addChild(tile);

    tile.fade(0, 1, 100+Math.random()*400, Phaser.Easing.Linear.None).onComplete.add(completeCallback, completeCallbackContext);
  }

}
