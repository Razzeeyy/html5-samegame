import {default as Array2} from '../utils/Array2';
// import {default as Array2} from '../utils/SafeArray2';
import AddTileAction from './actions/AddTileAction';
import FallTileAction from './actions/FallTileAction';
import SlideTileAction from './actions/SlideTileAction';
import RemoveTileAction from './actions/RemoveTileAction';

export default class BoardView extends Phaser.Group {

  constructor(game, board, tileSize) {
    super(game);
    this._board = board;
    this._TILE_SIZE = tileSize;
    this._tiles = new Array2(board.width, board.height);
    this._activeTweens = 0;
    this._actions = [];
    this._inputActive = true;

    board.events.onTileAdd.add(this._addTile, this);
    board.events.onTileFall.add(this._fallTile, this);
    board.events.onTileSlide.add(this._slideTile, this);
    board.events.onTileRemove.add(this._removeTile, this);
  }

  destroy(destroyChildren, soft) {
    super.destroy(destroyChildren, soft);

    const board = this._board;
    board.events.onTileAdd.remove(this._addTile, this);
    board.events.onTileFall.remove(this._fallTile, this);
    board.events.onTileSlide.remove(this._slideTile, this);
    board.events.onTileRemove.remove(this._removeTile, this);
  }

  isAnimating() {
    return this._activeTweens || this._actions.length;
  }

  isInputActive() {
    return this._inputActive && !this._activeTweens;
  }

  match(tileView) {
    if(!this.isInputActive()) return;

    const x = tileView.data.x;
    const y = tileView.data.y;

    this._board.match(x, y);
  }

  update() {
    super.update();

    if(this._actions.length && !this._activeTweens) {
      const type = this._actions[0].type;
      while(this._actions.length && this._actions[0].type === type) {
        const action = this._actions.shift();
        this._runAction(action);
      }
    }
  }

  _addTile(x, y, color) {
    this._actions.push(new AddTileAction(this, x, y, color));
  }

  _fallTile(x, y, toY) {
    this._actions.push(new FallTileAction(this, x, y, toY));
  }

  _slideTile(x, y, toX) {
    this._actions.push(new SlideTileAction(this, x, y, toX));
  }

  _removeTile(x, y) {
    this._actions.push(new RemoveTileAction(this, x, y));
  }

  _tweenComplete() {
    this._activeTweens--;
  }

  _runAction(action) {
    this._activeTweens++;
    action.run(this._tweenComplete, this);
  }

  setInputActive(isActive) {
    this._inputActive = isActive;
    for(let x=0; x<this._tiles.width; x++) {
      for(let y=0; y<this._tiles.height; y++) {
        const tileView = this._tiles.get(x, y);
        if(tileView) {
          tileView.input.enabled = isActive;
        }
      }
    }
  }
}
