export default class TileView extends Phaser.Button {

  constructor(game, x, y, color, size, cb, cb_ctx) {
    super(game, 0, 0, 'tile', cb, cb_ctx);
    this.tint = color;
    this.width = size;
    this.height = size;
    this.tileSize = size;

    this.setPosition(x, y);
    this.data.color = color;
  }

  setPosition(tileX, tileY) {
    this.x = tileX*this.tileSize;
    this.y = tileY*this.tileSize;

    this.data.x = tileX;
    this.data.y = tileY;
  }

  move(toX, toY, time, easing) {
    this.data.x = toX;
    this.data.y = toY;

    return this._tween({x: toX*this.tileSize, y: toY*this.tileSize}, time, easing);
  }

  fade(startAlpha, endAlpha, time, easing) {
    this.alpha = startAlpha;

    return this._tween({alpha: endAlpha}, time, easing);
  }

  _tween(params, time, easing) {
    const tween = this.game.add.tween(this);
    tween.to(params, time, easing, true);
    return tween;
  }
}
