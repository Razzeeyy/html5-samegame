import {default as Array2} from '../utils/SafeArray2';
import TileView from './TileView';

export default class ImmediateBoardView extends Phaser.Group {

  constructor(game, board, tileSize) {
    super(game);
    this._board = board;
    this._TILE_SIZE = tileSize;
    this._tiles = new Array2(board.width, board.height);

    board.events.onTileAdd.add(this._addTile, this);
    board.events.onTileFall.add(this._fallTile, this);
    board.events.onTileSlide.add(this._slideTile, this);
    board.events.onTileRemove.add(this._removeTile, this);
  }

  destroy(destroyChildren, soft) {
    super.destroy(destroyChildren, soft);

    const board = this._board;
    board.events.onTileAdd.remove(this._addTile, this);
    board.events.onTileFall.remove(this._fallTile, this);
    board.events.onTileSlide.remove(this._slideTile, this);
    board.events.onTileRemove.remove(this._removeTile, this);
  }

  match(tileView) {
    const x = tileView.data.x;
    const y = tileView.data.y;
    this._board.match(x, y);
  }

  _addTile(x, y, color) {
    const tile = new TileView(this.game, x, y, color, this._TILE_SIZE, this.match, this);
    this._tiles.set(x, y, tile);
    this.addChild(tile);
  }

  _fallTile(x, y, toY) {
    const tile = this._tiles.get(x, y);
    this._tiles.set(x, y, null);
    tile.setPosition(x, toY);
    this._tiles.set(x, toY, tile);
  }

  _slideTile(x, y, toX) {
    const tile = this._tiles.get(x, y);
    this._tiles.set(x, y, null);
    tile.setPosition(toX, y);
    this._tiles.set(toX, y, tile);
  }

  _removeTile(x, y) {
    const tile = this._tiles.get(x, y);
    this._tiles.set(x, y, null);
    this.remove(tile, true);
  }
}
